/**
 *  \brief     oop lab homework 1
 *  \details   Takes in a file with numbers and outputs the average, sum, product, minimum of said numbers
 *  \author    Bora Soylu
 *  \version   1.0
 *  \date      2020
 *  \warning   No warnings
 *  \copyright Free to use.
 */

#include <iostream>
#include <fstream>
#include <string>
#include <limits>
using namespace std;
void print_error(void); //prints an error message for invalid files
bool isnumber(string str);//returns true if current string only consists of digits

int main(void)
{
	string filename; //string for filename
	cout << "Filename: "; //take in filename from user
	cin >> filename;

	ifstream file(filename); //open file

	if (!file.is_open()) //check if file opened successfully
	{
		cout << endl << "Could not open file! ";
		cout << "Please check if file is in correct directory and ";
		cout << "if you've typed the correct filename.\n";
		return(1);
	}

	while (!file.eof()) // check if file has non numeric characters
	{
		string a;
		file >> a;
		if (!isnumber(a))
		{
			cout << "The file must only consist of numbers\n";
			print_error();
			return(1);
		}
	}
	file.clear();
	file.seekg(0);//reset file pointer
	int num_amount; //int for number amount
	file >> num_amount; //take in how many numbers there are
	int real_num_amount = 0; //check if file number count is correct
	while (!file.eof())
	{
		string a;
		file >> a;
		real_num_amount++;
	}
	if (real_num_amount != num_amount)
	{
		cout << "Number amount incorrect!\n";
		print_error();
		return(1);
	}
	file.clear();
	file.seekg(0);//reset file pointer
	double* numbers; //pointer for array
	numbers = new double[num_amount]; //allocate memmory for array
	file >> num_amount;//move file pointer forward
	for (int i = 0; i < num_amount; i++) //iterate over file
	{
		file >> numbers[i]; //fill array with numbers from file
	}


	double product = 1; //variable for product
	double sum = 0; //variable for sum
	double min = std::numeric_limits<double>::max(); //variable for min

	for (int i = 0; i < num_amount; i++) //iterate over array
	{
		product = product * numbers[i]; //calculate product
		sum = sum + numbers[i]; //sum all numbers
		if (numbers[i] < min) //find minimum numbers
		{
			min = numbers[i]; //assign new min
		}
	}
	cout << "Sum is: " << sum << endl; //print average
	cout << "Product is: " << product << endl; //print product
	cout << "Average is: " << sum / num_amount << endl; //print average
	cout << "Smallest is: " << min << endl; //print smallest


	delete[] numbers; //release memory
	file.close(); //close file
	return 0;
}

bool isnumber(string str) /**returns true if current string only consists of digits*/
{
	for (int i = 0, j = str.length(); i < j; i++)
	{
		if (!isdigit(str[i]))
		{
			return false;
		}
	}
	return true;
}

void print_error(void) /**prints an error message for invalid files*/
{
	cout << "Invalid file type!\n";
	cout << "Usage: <amount_of_numbers> ";
	cout << "<number1> <number2> <number3> .... <number n>\n";

}