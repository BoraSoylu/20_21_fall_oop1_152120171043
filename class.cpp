#include <iostream>
#include <sstream>
using namespace std;

class Student{
    public:
        void set_age(int a)
        {
            stringstream ss;  
            ss << a;  
            ss >> age;  
        }
        void set_standard(int a)
        {
            stringstream ss;  
            ss << a;  
            ss >> standard;  
        }
        void set_first_name(string a)
        {
            first_name = a;
        }
        void set_last_name(string a)
        {
            last_name = a;
        }
        string get_age(void)
        {
            return age;
        }
        string get_last_name(void)
        {
            return last_name;
        }
        string get_first_name(void)
        {
            return first_name;
        }
        string get_standard(void)
        {
            return standard;
        }
        string to_string(void)
        {
            return age + "," + first_name + "," + last_name + "," + standard;
            
        }
    private:
    string age;
    string standard;
    string first_name;
    string last_name;
};

int main() {
    int age, standard;
    string first_name, last_name;
    
    cin >> age >> first_name >> last_name >> standard;
    
    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);
    
    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();
    
    return 0;
}
